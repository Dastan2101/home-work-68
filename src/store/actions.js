import axios from 'axios';

export const ADDING = 'ADDING';
export const REMOVE = 'REMOVE';

export const FETCH_TASKS_REQUEST = 'FETCH_TASKS_REQUEST';
export const FETCH_TASKS_SUCCESS = 'FETCH_TASKS_SUCCESS';
export const FETCH_TASKS_ERROR = 'FETCH_TASKS_ERROR';


export const tasksRequest = () => {
    return {type: FETCH_TASKS_REQUEST};
};
export const tasksSuccess = (task) => {
    return {type: FETCH_TASKS_SUCCESS, task};
};
export const tasksError = () => {
    return {type: FETCH_TASKS_ERROR};
};

export const gettingTasks = () => {
  return dispatch => {
      dispatch(tasksRequest());
      axios.get('https://burger-project-dastan.firebaseio.com/tasks.json').then(response => {
          dispatch(tasksSuccess(response.data));
      }, error => {
          dispatch(tasksError())
      })
      }
};

export const addingTask = (task) => {
    return dispatch => {
        dispatch(tasksRequest());

        axios.post('https://burger-project-dastan.firebaseio.com/tasks.json', task).then(() => {
            dispatch(gettingTasks())
        });
    }
};

export const deleteTask = (id) => {
    return dispatch => {
        dispatch(tasksRequest());

        axios.delete('https://burger-project-dastan.firebaseio.com/tasks/' + id + '.json').then(() => {
            dispatch(gettingTasks())
        });
    }
};