import {FETCH_TASKS_SUCCESS} from "./actions";

const initialStore = {
    tasks: {},
};

const reducer = (state = initialStore, action) => {

    switch (action.type) {
        case FETCH_TASKS_SUCCESS:
            return {
                ...state,
                tasks: action.task
            };

        default:
            return state
    }
};

export default reducer;