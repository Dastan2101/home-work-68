import React, { Component } from 'react';
import AddTaskForm from '../components/AddTaskForm/AddTaskForm_component';
import Task from '../components/Task/Task_component';
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">

          <AddTaskForm onChange={(event) => this.changeHandler(event)}
                       addTasks={() => this.addTasks()}
          />

          <Task state={this.state}
                deleteTask={this.deleteTask}
                done={this.done}
          />

      </div>
    );
  }
}

export default App;