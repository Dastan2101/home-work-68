import React, {Component} from 'react';
import './block-task.css';
import {deleteTask, gettingTasks} from "../../store/actions";
import {connect} from "react-redux";

class Task extends Component {

    componentDidMount() {
        this.props.getTasks()
    }

    deleteHandler = (id) => {

        this.props.deleteTask(id)
    };


    render() {
        if (!this.props.tasks) {
            return <div>Loading...</div>
        } else {


       const tasks= Object.keys(this.props.tasks).map(id => {
            return {...this.props.tasks[id], id}
        });
        const task = tasks.map((task) => {
            return (
                    <div className="block-task" key={task.id}>

                    <p className="text">
                        {task.task}
                    </p>
                        <button className="delete-btn" onClick={() => this.deleteHandler(task.id)}>Delete</button>

                    </div>

                )
        });
        return (


            <div className="task">
                    {task}
            </div>
        )

        }
    }
}

const mapStateToProps = state => {
    return {
        tasks: state.tasks,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getTasks: () => dispatch(gettingTasks()),
        deleteTask: (id) => dispatch(deleteTask(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Task);
