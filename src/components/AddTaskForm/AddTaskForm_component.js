import React, {Component} from 'react';
import './addTask.css';
import {addingTask, gettingTasks} from "../../store/actions";
import {connect} from "react-redux";

class AddTask extends Component{

    state = {
        task: ''
    };

    changeHandler = (event) => {
        let value = event.target.value;


        this.setState({task: value})
    };

    // componentDidUpdate(prevProps, prevState) {
    //     if (prevState.task !== this.state.task) {
    //         this.props.getTasks()
    //     }
    // }

    addHandler = () => {
        const task = {
            task: this.state.task
        };
        this.props.addingTask(task)
    };

    render() {

        return (
            <div className="main-block">

                <input type="text" placeholder="Add new task" value={this.state.task} id="input"  onChange={this.changeHandler}/>
                <button id="btn" onClick={this.addHandler}>Add</button>

            </div>
        );
    }

}
const mapStateToProps = state => {
    return {
        tasks: state.tasks,
    }
};

const mapDispatchToProps = dispatch => {

    return {
        addingTask: (task) => dispatch(addingTask(task)),
        getTasks: () => dispatch(gettingTasks()),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);












